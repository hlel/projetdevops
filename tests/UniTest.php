<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Client;
use App\Entity\Location;
use App\Entity\Modele;
use App\Entity\User;
use App\Entity\Voiture;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UniTest extends TestCase
{
/***********client*************/
    public function testClient(): void
    {
        $client = new Client();
        // Testez les setters et les getters
        $client->setNom("Mohamed Aziz");
        $this->assertSame("Mohamed Aziz", $client->getNom(), "The getter or setter for the nom is not working.");

        $client->setPrenom("Hlel");
        $this->assertSame("Hlel", $client->getPrenom(), "The getter or setter for the prenom is not working.");

        $client->setAdresse("13 Rue Barcelona");
        $this->assertSame("13 Rue Barcelona", $client->getAdresse(), "The getter or setter for the adress is not working.");

        $client->setCin("12345678");
        $this->assertSame("12345678", $client->getCin(), "The getter or setter for the cin is not working.");

    }
/***********location*************/
    public function testLocation(): void
    {
        $location = new Location();

        // Tester les setters et getters pour les dates
        $dateDebut = new \DateTime('2024-01-02');
        $location->setDateD($dateDebut);
        $this->assertSame($dateDebut, $location->getDateD(), "The getter or setter for the dateDebut is not working.");

        $dateFin = new \DateTime('2024-01-05');
        $location->setDateA($dateFin);
        $this->assertSame($dateFin, $location->getDateA(), "The getter or setter for the dateFin is not working.");

        // Tester le setter et le getter pour le prix
        $location->setPrix(100.0);
        $this->assertSame(100.0, $location->getPrix(), "The getter or setter for the prix is not working.");

        // Tester la relation avec l'entité Client
        $client = new Client();
        $location->setClient($client);
        $this->assertSame($client, $location->getClient(), "The relationship with the Client entity is not properly managed.");

        // Tester la relation avec l'entité Voiture
        $voiture = new Voiture();
        $location->setVoiture($voiture);
        $this->assertSame($voiture, $location->getVoiture(), "The relationship with the Voiture entity is not properly managed.");

        // verifier l'instance est du bon type
        $this->assertInstanceOf(Location::class, $location, "The instance is not of type Location.");
    }

/***********voiture*************/

    public function testVoiture(): void
    {
        $voiture = new Voiture();

        // Tester les setters et getters pour la série
        $voiture->setSerie('100TUN236');
        $this->assertSame('100TUN236', $voiture->getSerie(), "The getter or setter for the serie is not working.");

        // Tester les setters et getters pour la date de mise en marché
        $dateMM = new \DateTime('2019-02-15');
        $voiture->setDateMM($dateMM);
        $this->assertSame($dateMM, $voiture->getDateMM(), "The getter or setter for the dateMM is not working.");

        // Tester le setter et le getter pour le prix par jour
        $voiture->setPrixJour(40.0);
        $this->assertSame(40.0, $voiture->getPrixJour(), "The getter or setter for the PrixJour is not working.");

        // Tester l'ajout et la suppression de locations
        $location = new Location();
        $voiture->addLocation($location);
        $this->assertCount(1, $voiture->getLocations(), "Adding the Location to the collection failed.");

        $voiture->removeLocation($location);
        $this->assertCount(0, $voiture->getLocations(), "Removing the tenancy from the collection failed.");

        // Tester la relation avec l'entité Modele
        $modele = new Modele();
        $voiture->setModele($modele);
        $this->assertSame($modele, $voiture->getModele(), "The relationship with the Modele entity is not properly managed.");

        // verif  l'instance est du bon type
        $this->assertInstanceOf(Voiture::class, $voiture, "The instance is not of type Voiture.");
    }

/***********user*************/
    public function testUser(): void
    {
        $user = new User();

        // Tester le setter et le getter pour l'email
        $user->setEmail('hlel@example.com');
        $this->assertSame('hlel@example.com', $user->getEmail(), "The getter or setter for the email is not working.");

        // Tester le UserIdentifier (qui est également l'email dans ce cas)
        $this->assertSame('hlel@example.com', $user->getUserIdentifier(), "The getUserIdentifier method does not return the correct identifier.");

        // Tester les setters et getters pour les roles
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];
        $user->setRoles($roles);
        $this->assertContains('ROLE_USER', $user->getRoles(), "The ROLE_USER role must always be present.");
        $this->assertContains('ROLE_ADMIN', $user->getRoles(), "The ROLE_ADMIN role must be defined by the setter.");

        // Tester le setter et le getter pour le mot de passe
        $user->setPassword('hashed_password');
        $this->assertSame('hashed_password', $user->getPassword(), "The getter or setter for the password is not working.");


        // Assurez-vous que l'instance est du bon type
        $this->assertInstanceOf(UserInterface::class, $user, "The User instance does not implement UserInterface.");
        $this->assertInstanceOf(PasswordAuthenticatedUserInterface::class, $user, "The User instance does not implement PasswordAuthenticatedUserInterface.");
    }
/*************modele**************/
    public function testModele(): void
    {
        $modele = new Modele();

        // Tester les setters et getters pour libelle
        $modele->setLibelle('Audi');
        $this->assertSame('Audi', $modele->getLibelle(), "The getter or setter for the libelle is not working.");

        // Tester les setters et getters pour pays
        $modele->setPays('Allemagne');
        $this->assertSame('Allemagne', $modele->getPays(), "The getter or setter for the pays is not working.");

        // Tester l'ajout et la suppression de voitures
        $voiture = new Voiture();
        $modele->addVoiture($voiture);
        $this->assertCount(1, $modele->getVoitures(), "Adding the Voiture to the collection failed.");
        $this->assertTrue($modele->getVoitures()->contains($voiture), "The Voiture must be contained in the collection after addition.");

        $modele->removeVoiture($voiture);
        $this->assertCount(0, $modele->getVoitures(), "Removing the Voiture from the collection failed.");
        $this->assertFalse($modele->getVoitures()->contains($voiture), "The Voiture should not be contained in the collection after deletion.");

        // VERIFIER l'instance est du bon type
        $this->assertInstanceOf(Modele::class, $modele, "The instance is not of type Modele.");
    }


}

